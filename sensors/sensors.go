package main

import "time"

// SensorValue is a single sensor reading, including a type and unit (e.g. "C" or "PPM").
type SensorValue struct {
	Value     float64
	Type      string
	Unit      string
	Timestamp time.Time
}

// A Sensor is an object that can measure a value at a moment in time.
type Sensor interface {
	Init() error
	GetValue() ([]SensorValue, error)
	Stop() error
}
