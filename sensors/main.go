package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

var sensors = []Sensor{
	// &AM2315{},
	// &SerialDHT22{},
	&SHT31D{},
}

func main() {
	for _, s := range sensors {
		err := s.Init()
		if err != nil {
			log.Printf("Error during sensor initialization: %v\n", err)
		}
	}

	http.HandleFunc("/", getSensorValues)
	log.Fatal(http.ListenAndServe(":8182", nil))
}

func getSensorValues(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	errs := []error{}
	vals := []SensorValue{}
	for _, s := range sensors {
		val, err := s.GetValue()
		if err != nil {
			errs = append(errs, err)
		} else {
			for _, v := range val {
				vals = append(vals, v)
			}
		}
	}

	if len(errs) > 0 {
		http.Error(w, fmt.Sprint(errs), http.StatusInternalServerError)
		return
	}

	jsonData, err := json.Marshal(vals)
	if err != nil {
		http.Error(w, fmt.Sprintf("Error encoding json: %v", err), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonData)
}
