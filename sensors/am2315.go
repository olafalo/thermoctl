package main

import (
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"periph.io/x/periph/conn/i2c"
	"periph.io/x/periph/conn/i2c/i2creg"
	"periph.io/x/periph/host"
)

// AM2315 is a combined temperature and humidity sensor with values that
// are sent via I2C.
type AM2315 struct {
	CurrentTemp     float64
	CurrentHumidity float64
	LastReadTime    time.Time
	valuesLock      sync.Mutex
	ready           bool
}

// Init begins reading from the I2C interface.
func (s *AM2315) Init() error {
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	getRawData := func() (temp float64, humidity float64, err error) {
		// Open bus
		b, err := i2creg.Open("1")
		if err != nil {
			return 0, 0, err
		}
		defer b.Close()

		// Address: 0x5C
		d := &i2c.Dev{Addr: 0x5c, Bus: b}

		// write zero byte to wake up
		d.Write([]byte{0})

		// write a read request (3,0,4)
		_, err = d.Write([]byte{3, 0, 4})
		if err != nil {
			return 0, 0, err
		}

		// read response (8 bytes)
		response := make([]byte, 8)
		err = d.Tx(nil, response)
		if err != nil {
			return 0, 0, err
		}

		if response[0] != 3 || response[1] != 4 {
			fmt.Println("Garbage data, skipping")
		}

		// Temp and humidity are high byte first
		humidHigh := uint16(response[2])
		humidLow := uint16(response[3])
		tempHigh := uint16(response[4])
		tempLow := uint16(response[5])

		temp = float64(tempHigh<<8+tempLow) / 10
		humidity = float64(humidHigh<<8+humidLow) / 10

		// Checksum is low byte first
		crc := uint16(response[6]) + uint16(response[7])<<8

		// Generate expected checksum from received data (CRC-16)
		// https://cdn-shop.adafruit.com/datasheets/AM2315.pdf
		var expectedCRC uint16 = 0xFFFF
		for _, dataByte := range response[0:6] {
			expectedCRC ^= uint16(dataByte)
			for i := 0; i < 8; i++ {
				if expectedCRC&0x01 == 0x01 {
					expectedCRC >>= 0x1
					expectedCRC ^= 0xA001
				} else {
					expectedCRC >>= 0x1
				}
			}
		}

		if expectedCRC != crc {
			return 0, 0, fmt.Errorf("Checksum: Expected %d, got %d", expectedCRC, crc)
		}

		return temp, humidity, nil
	}

	go func() {
		for {
			time.Sleep(2 * time.Second)
			temp, humidity, err := getRawData()
			if err != nil {
				// AM2315 generates too many errors for us to log them, just continue.
				// If data gets too stale the manager won't use it anyway.
				continue
			}

			// Update last values
			s.valuesLock.Lock()
			s.CurrentTemp = temp
			s.CurrentHumidity = humidity
			s.LastReadTime = time.Now()
			s.ready = true
			s.valuesLock.Unlock()
		}
	}()
	return nil
}

// GetValue returns temperature and humidity readings.
func (s *AM2315) GetValue() ([]SensorValue, error) {
	if !s.ready {
		return nil, errors.New("Sensor not ready")
	}
	s.valuesLock.Lock()
	values := sensorValues(s.CurrentTemp, s.CurrentHumidity, s.LastReadTime)
	s.valuesLock.Unlock()
	return values, nil
}

// Stop does nothing, the bus is already closed after every read.
func (s *AM2315) Stop() error {
	return nil
}
