package main

import (
	"bufio"
	"errors"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

// SerialDHT22 is a combined temperature and humidity sensor with values that
// are sent via UART/serial.
type SerialDHT22 struct {
	CurrentTemp     float64
	CurrentHumidity float64
	LastReadTime    time.Time
	valuesLock      sync.Mutex
	serial          *os.File
	ready           bool
}

// Transform plain values into SensorValues.
func sensorValues(temp, humidity float64, timestamp time.Time) []SensorValue {
	return []SensorValue{
		SensorValue{
			Value:     float64(temp),
			Type:      "Temperature",
			Unit:      "C",
			Timestamp: timestamp,
		},
		SensorValue{
			Value:     float64(humidity),
			Type:      "Relative Humidity",
			Unit:      "%",
			Timestamp: timestamp,
		},
	}
}

// Init begins reading from the serial port.
func (s *SerialDHT22) Init() error {
	var err error
	s.serial, err = os.Open("/dev/ttyS0")
	if err != nil {
		return err
	}
	go func() {
		for {
			// Read data from serial
			buf := bufio.NewReader(s.serial)
			data, err := buf.ReadString('\n')
			data = strings.Trim(data, "\n")
			if err != nil {
				log.Fatal("Serial interface closed!")
			}

			// Parse data
			values := strings.Split(data, " ")
			if len(values) < 2 {
				log.Printf("Not enough values: %s\n", data)
				continue
			}
			temp, terr := strconv.ParseFloat(values[0], 64)
			humid, herr := strconv.ParseFloat(values[1], 64)
			if terr != nil || herr != nil {
				log.Printf("Bad serial input: %s (%v %v)\n", data, terr, herr)
				continue
			}

			// Update last values
			s.valuesLock.Lock()
			s.CurrentTemp = temp
			s.CurrentHumidity = humid
			s.LastReadTime = time.Now()
			s.ready = true
			s.valuesLock.Unlock()
		}
	}()
	return nil
}

// GetValue returns temperature and humidity readings.
func (s *SerialDHT22) GetValue() ([]SensorValue, error) {
	if !s.ready {
		return nil, errors.New("Sensor not ready")
	}
	s.valuesLock.Lock()
	values := sensorValues(s.CurrentTemp, s.CurrentHumidity, s.LastReadTime)
	s.valuesLock.Unlock()
	return values, nil
}

// Stop closes the serial interface.
func (s *SerialDHT22) Stop() error {
	return s.serial.Close()
}
