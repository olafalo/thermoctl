package main

import (
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"periph.io/x/periph/conn/i2c"
	"periph.io/x/periph/conn/i2c/i2creg"
	"periph.io/x/periph/host"
)

// SHT31D is a combined temperature and humidity sensor with values that
// are sent via I2C.
type SHT31D struct {
	CurrentTemp     float64
	CurrentHumidity float64
	LastReadTime    time.Time
	valuesLock      sync.Mutex
	ready           bool
	bus             i2c.BusCloser
	device          *i2c.Dev
}

// Init begins reading from the I2C interface.
func (s *SHT31D) Init() (err error) {
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	s.bus, err = i2creg.Open("")
	if err != nil {
		log.Fatal(err)
	}

	s.device = &i2c.Dev{Addr: 0x44, Bus: s.bus}

	s.ready = true

	return nil
}

func shtCRC8(data []byte) byte {
	polynomial := byte(0x31)
	crc := byte(0xff)
	for j := 0; j < len(data); j++ {
		crc ^= data[j]

		for i := 0; i < 8; i++ {
			if crc&0x80 != 0 {
				crc <<= 1
				crc ^= polynomial
			} else {
				crc <<= 1
			}
		}
	}
	return crc
}

// GetValue returns temperature and humidity readings.
func (s *SHT31D) GetValue() ([]SensorValue, error) {
	if !s.ready {
		return nil, errors.New("Sensor not ready")
	}

	// Write one-shot high repeatability measurement command 0x2c06
	request := []byte{0x2c, 0x06}
	response := make([]byte, 6)
	err := s.device.Tx(request, response)
	if err != nil {
		return nil, err
	}

	// Calculate CRC-8 checksums
	tempCRC := shtCRC8(response[0:2])
	humidCRC := shtCRC8(response[3:5])

	if tempCRC != response[2] {
		err = fmt.Errorf("temperature checksum %x was not equal to expected value %x", response[2], tempCRC)
		return nil, err
	}

	if humidCRC != response[5] {
		err = fmt.Errorf("humidity checksum %x was not equal to expected value %x", response[5], humidCRC)
		return nil, err
	}

	// Temp and humidity are high byte first
	tempHigh := uint16(response[0])
	tempLow := uint16(response[1])
	humidHigh := uint16(response[3])
	humidLow := uint16(response[4])

	temp := -45 + 175*(float64(tempHigh<<8+tempLow)/(65536-1))
	humidity := 100 * (float64(humidHigh<<8+humidLow) / (65536 - 1))

	return sensorValues(temp, humidity, time.Now()), nil
}

// Stop does nothing, the bus is already closed after every read.
func (s *SHT31D) Stop() error {
	return s.bus.Close()
}
