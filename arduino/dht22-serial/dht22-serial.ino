// DHT Temperature & Humidity Sensor
// Unified Sensor Library Example
// Written by Tony DiCola for Adafruit Industries
// Released under an MIT license.

// REQUIRES the following Arduino libraries:
// - DHT Sensor Library: https://github.com/adafruit/DHT-sensor-library
// - Adafruit Unified Sensor Lib: https://github.com/adafruit/Adafruit_Sensor

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 8     // Digital pin connected to the DHT sensor 
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment the type of sensor in use:
//#define DHTTYPE    DHT11     // DHT 11
#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

// See guide for details on sensor wiring and usage:
//   https://learn.adafruit.com/dht/overview

DHT_Unified dht(DHTPIN, DHTTYPE);

uint32_t delayMS;

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  // Delay between measurements.
  delay(2000);
  // Get temperature event and print its value.
  sensors_event_t temp_event;
  dht.temperature().getEvent(&temp_event);
  sensors_event_t humid_event;
  dht.humidity().getEvent(&humid_event);
  if (isnan(temp_event.temperature) || isnan(humid_event.temperature)) {
    Serial.print("ERROR\n");
  }
  else {
    Serial.print(temp_event.temperature);
    Serial.print(" ");
    Serial.print(humid_event.relative_humidity);
    Serial.print("\n");
  }
}
