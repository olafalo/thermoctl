package main

import "github.com/stianeikeland/go-rpio"

const compressorPin = 21
const fanPin = 20
const pumpPin = 26

// HVACState represents a state that the switches for an HVAC unit can be in.
type HVACState struct {
	CondenserState bool
	FanState       bool
	HeatPumpState  bool
}

func (s *HVACState) String() string {
	switch *s {
	case offState:
		return "off"
	case acState:
		return "cool"
	case fanState:
		return "fan"
	case heatState:
		return "heat"
	default:
		return "unknown"
	}
}

// AC: Y + G + O
var acState = HVACState{
	CondenserState: true,
	FanState:       true,
	HeatPumpState:  true,
}

// FAN: G
var fanState = HVACState{
	CondenserState: false,
	FanState:       true,
	HeatPumpState:  false,
}

// HEAT: Y + G (energize compressor, energize blower, do not energize reversing
// valve in heat pump)
var heatState = HVACState{
	CondenserState: true,
	FanState:       true,
	HeatPumpState:  false,
}

// OFF: Disconnected
var offState = HVACState{
	CondenserState: false,
	FanState:       false,
	HeatPumpState:  false,
}

func gpioInit() {
	rpio.Pin(compressorPin).Output()
	rpio.Pin(fanPin).Output()
	rpio.Pin(pumpPin).Output()
	stateChange <- offState
}

func setState(state HVACState) {
	// Relay module is active high.
	if !gpioEnabled {
		return
	}
	// Set the condensor
	if state.CondenserState {
		rpio.Pin(compressorPin).High()
	} else {
		rpio.Pin(compressorPin).Low()
	}

	// Set the fan
	if state.FanState {
		rpio.Pin(fanPin).High()
	} else {
		rpio.Pin(fanPin).Low()
	}

	// Set the heat pump
	if state.HeatPumpState {
		rpio.Pin(pumpPin).High()
	} else {
		rpio.Pin(pumpPin).Low()
	}
}
