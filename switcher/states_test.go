package main

import (
	"testing"
)

func TestStateToString(t *testing.T) {
	if acState.String() != "cool" {
		t.Fail()
	}
	if fanState.String() != "fan" {
		t.Fail()
	}
	if heatState.String() != "heat" {
		t.Fail()
	}
	if offState.String() != "off" {
		t.Fail()
	}
	unknownState := HVACState{
		HeatPumpState: true,
	}
	if unknownState.String() != "unknown" {
		t.Fail()
	}
}
