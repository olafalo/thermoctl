package main

import (
	"switcher/timing"
	"testing"
	"time"
)

func TestStateTimeout(t *testing.T) {
	gpioEnabled = false
	clock = timing.NewTestClock()

	// Start state change handler and timeout watchdog
	go manageStateChanges()
	go disableOnTimeout()

	// turn the heat on
	stateChange <- heatState

	// Fake sleep to simulate a timeout
	clock.Sleep(stateTimeout * time.Second)

	// Small real sleep to make sure the state is changed
	time.Sleep(100 * time.Millisecond)

	// check that current state is off
	if currentState != offState {
		t.Errorf("Current state should be off, was %v\n", currentState)
	}
}
