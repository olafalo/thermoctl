package main

import (
	"log"
	"net/http"
	"switcher/timing"
	"time"

	"github.com/stianeikeland/go-rpio"
)

var clock timing.Clock = &timing.TimeClock{}
var gpioEnabled = true

var currentState = offState
var lastUpdate time.Time
var stateTimeout = 600 * time.Second
var stateChange = make(chan HVACState)
var watchRate = 60 * time.Second

func main() {
	// Init state change handler and timeout watchdog
	go manageStateChanges()
	go disableOnTimeout()

	// Init GPIO pins
	rpio.Open()
	defer rpio.Close()
	gpioInit()

	// In case of panic (or any exit), attempt to turn off
	defer func() {
		setState(offState)
	}()

	// Start web server
	http.HandleFunc("/", baseHandler)
	err := http.ListenAndServe(":8181", nil)
	if err != nil {
		stateChange <- offState
		log.Fatal(err)
	}
}

// manageStateChanges synchronizes changes to state.
func manageStateChanges() {
	for newState := range stateChange {
		lastUpdate = clock.Now()
		currentState = newState
		setState(newState)
		log.Printf("Set state %s\n", newState.String())
	}
}

// disableOnTimeout sets the state to off if the state hasn't been updated in
// a certain period of time.
func disableOnTimeout() {
	for {
		if clock.Since(lastUpdate) > stateTimeout && currentState != offState {
			log.Printf("WARNING: Timeout reached, disabling...")
			stateChange <- offState
		}
		clock.Sleep(watchRate)
	}
}

func baseHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}
		// Return current state
		w.Write([]byte(currentState.String()))
		return
	}

	if r.Method != "POST" {
		http.Error(w, "Invalid method", http.StatusMethodNotAllowed)
		return
	}

	switch r.URL.Path {
	case "/" + acState.String():
		currentState = acState
	case "/" + fanState.String():
		currentState = fanState
	case "/" + heatState.String():
		currentState = heatState
	case "/" + offState.String():
		currentState = offState
	default:
		http.Error(w, "Invalid state", http.StatusNotFound)
		return
	}
	stateChange <- currentState
	w.WriteHeader(200)
}
