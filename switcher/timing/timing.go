// Package timing provides an interface that the `time` standard library fulfills.
// This provides an easy way to mock out the time package for tests that depend
// on sleeps, delays, and time measurement.
package timing

import "time"

// The Clock interface represents parts of the time library.
type Clock interface {
	Now() time.Time
	Since(t time.Time) time.Duration
	Sleep(time.Duration)
}

// TimeClock provides the Clock interface backed by methods from the time package.
type TimeClock struct{}

// Now returns the current local time.
func (TimeClock) Now() time.Time {
	return time.Now()
}

// Since returns the time elapsed since t. It is shorthand for time.Now().Sub(t).
func (TimeClock) Since(t time.Time) time.Duration {
	return time.Since(t)
}

// Sleep pauses the current goroutine for at least the duration d.
// A negative or zero duration causes Sleep to return immediately.
func (tc TimeClock) Sleep(d time.Duration) {
	time.Sleep(d)
}

// TestClock provides a testing implementation of the Clock interface.
type TestClock struct {
	StartTime time.Time
}

// NewTestClock initializes a new TestClock instance.
func NewTestClock() *TestClock {
	return &TestClock{StartTime: time.Now()}
}

// Now returns time at which testing started, modified by any sleeps.
func (tc *TestClock) Now() time.Time {
	return tc.StartTime
}

// Since returns the time elapsed since the last recorded test time.
func (tc *TestClock) Since(t time.Time) time.Duration {
	return tc.Now().Sub(t)
}

// Sleep adds the requested duration to the time waited, and returns immediately.
func (tc *TestClock) Sleep(d time.Duration) {
	tc.StartTime = tc.StartTime.Add(d)
}
