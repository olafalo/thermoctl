package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sync"
	"time"
)

const pollDelay = time.Second * 5
const historyPeriod = time.Hour * 12
const historyFileCleanPeriod = time.Hour

var currentTemp float64
var currentHumidity float64
var recordHistory = newSeriesBuffer(historyPeriod)
var historyFileSync = &sync.Mutex{}

func init() {
	err := openRecordsFile()
	if err != nil {
		return
	}
	data, err := ioutil.ReadAll(records)
	records := bytes.Split(data, []byte{'\n'})
	for _, rec := range records {
		if len(rec) < 2 {
			continue
		}
		result := SensorRecord{}
		err = json.Unmarshal(rec, &result)
		if err != nil {
			log.Println(err)
			continue
		}
		recordHistory.push(result)
	}
}

func pollSensors() {
	for {
		time.Sleep(pollDelay)
		data := getSensorData()
		if data == nil {
			continue
		}
		var tempValue, humidValue *SensorValue = nil, nil
		for i, v := range data {
			if v.Type == "Temperature" {
				tempValue = &data[i]
			} else if v.Type == "Relative Humidity" {
				humidValue = &data[i]
			} else {
				log.Printf("Unknown sensor value type: %s\n", v.Type)
			}
		}
		if tempValue == nil || humidValue == nil {
			log.Printf("Couldn't get sensor values; data was %v\n", data)
			continue
		}

		record := SensorRecord{
			TempC:    tempValue.Value,
			Humidity: humidValue.Value,
			UnixMS:   tempValue.Timestamp.UnixNano() / 1e6,
		}

		recordHistory.push(record)
		appendData(tempValue.Value, humidValue.Value, tempValue.Timestamp)
	}
}

func currentSensorValues() (temp, humidity float64, err error) {
	lastRecord := recordHistory.getLastN(1)
	if lastRecord == nil || len(lastRecord) < 1 {
		return 0, 0, errors.New("Missing temperature or humidity records")
	}
	return lastRecord[0].TempC, lastRecord[0].Humidity, nil
}

func smoothedHistory(bucketSizeS int64, timeLimit time.Duration) []SensorRecord {
	hist := recordHistory.latestInTimeframe(timeLimit)
	start := hist[0].UnixMS
	avgData := make([]SensorRecord, 0)
	numInCurrentBucket := int64(0)

	current := &SensorRecord{}
	for _, r := range hist {
		if r.UnixMS > start+(bucketSizeS*1000) {
			// Time to start a new bucket
			// Average all summed values so far
			current.divide(numInCurrentBucket)
			numInCurrentBucket = 0
			start = r.UnixMS
			avgData = append(avgData, *current)
			current = &SensorRecord{}
		}

		current.add(r)
		numInCurrentBucket++
	}
	current.divide(numInCurrentBucket)
	avgData = append(avgData, *current)

	return avgData
}

func getCurrentOverPeriod(periodS int64) (*SensorRecord, error) {
	records := recordHistory.latestInTimeframe(time.Duration(periodS) * time.Second)
	if len(records) < 1 {
		return nil, errors.New("No records found in requested window")
	}

	// Average all records together
	val := SensorRecord{}
	var total int64
	for i := 0; i < len(records); i++ {
		total++
		val.add(records[i])
	}
	val.divide(total)
	return &val, nil
}

func getSensorData() []SensorValue {
	resp, err := http.Get("http://localhost:8182")
	if err != nil {
		log.Println(err)
		return nil
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		return nil
	}
	var data []SensorValue
	err = json.Unmarshal(body, &data)
	if err != nil {
		log.Println(err)
		return nil
	}
	return data
}

// SensorValue is a single sensor reading, including a type and unit (e.g. "C" or "PPM").
type SensorValue struct {
	Value     float64
	Type      string
	Unit      string
	Timestamp time.Time
}

// SensorRecord is the data structure used for saving to the log file.
type SensorRecord struct {
	TempC    float64 `json:"tc,omitempty"`
	Humidity float64 `json:"h,omitempty"`
	UnixMS   int64   `json:"t,omitempty"`
}

// Timestamp returns a standard time object from the record's timestamp
func (s *SensorRecord) Timestamp() time.Time {
	return time.Unix(s.UnixMS/1000, (s.UnixMS%1000)*1e6)
}

func (s *SensorRecord) add(s2 SensorRecord) {
	s.TempC += s2.TempC
	s.Humidity += s2.Humidity
	s.UnixMS += s2.UnixMS
}

func (s *SensorRecord) divide(n int64) {
	s.TempC = s.TempC / float64(n)
	s.Humidity = s.Humidity / float64(n)
	s.UnixMS = s.UnixMS / n
}

var records *os.File

func openRecordsFile() error {
	if records == nil {
		historyFileSync.Lock()
		defer historyFileSync.Unlock()
		var err error
		records, err = os.OpenFile("history.dat", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

func appendData(tempC, humidity float64, timestamp time.Time) {
	historyFileSync.Lock()
	defer historyFileSync.Unlock()
	err := openRecordsFile()

	if err != nil {
		log.Println(err)
		return
	}
	data := SensorRecord{
		TempC:    tempC,
		Humidity: humidity,
		UnixMS:   timestamp.UnixNano() / 1e6,
	}
	record, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return
	}
	records.Write(record)
	records.Write([]byte{'\n'})
}

func keepHistoryFileClean() {
	for {
		removeOldRecords()
		time.Sleep(historyFileCleanPeriod)
	}
}

// removeOldRecords rewrites the records file with only current data.
func removeOldRecords() {
	historyFileSync.Lock()
	defer historyFileSync.Unlock()
	err := records.Close()
	if err != nil {
		log.Println(err)
		return
	}

	// Create and truncate history file
	records, err = os.Create("history.dat")
	log.Println("Truncated history file")

	// Write all recent data
	recentData := recordHistory.getAll()
	for _, data := range recentData {
		rec, err := json.Marshal(data)
		if err != nil {
			log.Println(err)
			return
		}
		records.Write(rec)
		records.Write([]byte{'\n'})
	}
}
