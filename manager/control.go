package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var lastUpdate time.Time
var lastRefresh time.Time
var lastState string

const updateInterval = time.Second * 10
const refreshInterval = time.Second * 60
const tempBuffer = 0.5

func init() {
	// Starting the daemon should count as changing state - reset timer
	lastUpdate = time.Now()
	state, err := getState()
	if err != nil {
		log.Println(err)
		return
	}
	lastState = state
}

func stateControl() {
	// In case of panic, try to turn off
	defer func() {
		changeState("off")
	}()

	for {
		time.Sleep(updateInterval)

		err := updateState()
		if err != nil {
			log.Printf("Controller error: %v\n", err)
			err = changeState("off")
			if err != nil {
				// Critical error, failed to turn off - send SMS or something?
			}
		}
	}
}

func updateState() error {
	// If state changed too recently, skip updating
	if time.Since(lastUpdate) < (time.Second * time.Duration(settings.MinSwitchFreq)) {
		return nil
	}

	// Get average sensor values over the last N seconds
	current, err := getCurrentOverPeriod(60)
	if err != nil {
		// If this fails, we don't have current temperature data - turn off to be safe
		log.Printf("Can't get sensor data, disabling: %v\n", err)
		return changeState("off")
	}

	currentTempF := current.TempC*1.8 + 32

	stateStr, err := getState()
	if err != nil {
		log.Printf("Disabling, unknown state: %v\n", err)
		return changeState("off")
	}

	lastState = stateStr

	if !settings.Enabled {
		if stateStr != "off" {
			return changeState("off")
		}
		return nil
	}

	// Generally speaking, we should undershoot turning on
	// Meaning e.g. we don't turn on the AC until it's maxtemp + buffer,
	// and don't turn it off until it's minimum again
	// Another example, if min = max = 72 and buffer = 0.5:
	// Hotter than 72.5: turn on AC
	// Cooler than 72: turn off AC
	// Colder than 71.5: turn on heat
	// Warmer than 72: turn off heat
	if stateStr == "off" || stateStr == "fan" {
		if currentTempF > settings.MaxTemp+tempBuffer {
			return changeState("cool")
		}
		if currentTempF < settings.MinTemp-tempBuffer {
			return changeState("heat")
		}
		if settings.FanAlwaysOn {
			if stateStr == "off" {
				return changeState("fan")
			} else if stateStr == "fan" && time.Since(lastRefresh) > refreshInterval {
				// Ping switcher server to keep fan on
				return setState("fan")
			}
		}
		if stateStr == "fan" && !settings.FanAlwaysOn {
			return changeState("off")
		}
	} else if stateStr == "cool" {
		if currentTempF <= settings.MinTemp {
			if settings.FanAlwaysOn {
				return changeState("fan")
			}
			return changeState("off")
		}
		if time.Since(lastRefresh) > refreshInterval {
			// Ping switcher server to keep cooling on
			return setState("cool")
		}
	} else if stateStr == "heat" {
		if currentTempF >= settings.MaxTemp {
			if settings.FanAlwaysOn {
				return changeState("fan")
			}
			return changeState("off")
		}

		if time.Since(lastRefresh) > refreshInterval {
			// Ping switcher server to keep cooling on
			return setState("heat")
		}
	}

	return nil
}

func getState() (string, error) {
	resp, err := http.Get("http://localhost:8181")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func setState(state string) error {
	lastRefresh = time.Now()
	log.Println("Setting state to " + state)
	resp, err := http.Post("http://localhost:8181/"+state, "text/plain", nil)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf("Setting state resulted in status code %d", resp.StatusCode)
	}
	lastState = state
	return nil
}

func changeState(state string) error {
	lastUpdate = time.Now()
	return setState(state)
}
