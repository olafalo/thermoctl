package main

import (
	"errors"
	"time"
)

// seriesBuffer is a buffer of time-series data that supports transparent expiration
// and ordering guarantees.
type seriesBuffer struct {
	buf        []SensorRecord
	savePeriod time.Duration
}

// newSeriesBuffer initializes and returns a new seriesBuffer object.
func newSeriesBuffer(period time.Duration) *seriesBuffer {
	return &seriesBuffer{
		buf:        make([]SensorRecord, 0),
		savePeriod: period,
	}
}

func (sb *seriesBuffer) push(rec SensorRecord) error {
	// Go through buffer to find first non-expired record
	earliest := time.Now().Add(-sb.savePeriod)
	for i := 0; i < len(sb.buf); i++ {
		if sb.buf[i].Timestamp().After(earliest) {
			sb.buf = sb.buf[i:]
			break
		}
	}

	if len(sb.buf) > 1 && rec.Timestamp().Before(sb.buf[len(sb.buf)-1].Timestamp()) {
		return errors.New("Record's timestamp is earlier than latest data")
	}
	sb.buf = append(sb.buf, rec)
	return nil
}

func (sb *seriesBuffer) latestInTimeframe(period time.Duration) []SensorRecord {
	// Loop backwards through buffer to find record range
	var earliest int
	for i := len(sb.buf) - 1; i >= 0 && time.Since(sb.buf[i].Timestamp()) < period; i-- {
		earliest = i
	}

	return sb.buf[earliest:]
}

func (sb *seriesBuffer) getLastN(n int) []SensorRecord {
	start := len(sb.buf) - n
	if start < 0 {
		start = 0
	}
	return sb.buf[start:len(sb.buf)]
}

func (sb *seriesBuffer) getAll() []SensorRecord {
	return sb.buf
}
