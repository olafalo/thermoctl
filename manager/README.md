# Thermoctl: Manager

## Features (ordered)

- Display sensor values. Buttons to set system to COOL|FAN|HEAT|OFF.
- Set min and max temp, system switches on and off automatically.
- Show graph of sensor values and system state on home page.
- Track stats such as system running time (estimated power use?).
