export default {
  get: async (url, params?: Object) => {
    if (params) {
      const searchParams = new URLSearchParams();
      Object.entries(params).forEach(([k, v]) => searchParams.append(k, v));
      url = `${url}?${searchParams.toString()}`;
    }
    const response = await fetch(url, {
      method: 'GET',
      mode: 'cors',
    });
    if (response.status >= 300) {
      throw `${response.status}: ${response.statusText}`;
    }
    return response.json();
  },

  post: async (url, data) => {
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    if (response.status >= 300) {
      throw `${response.status}: ${response.statusText}`;
    }
    return response.json();
  },

  delete: async (url) => {
    const response = await fetch(url, {
      method: 'DELETE',
      mode: 'cors',
    });
    if (response.status >= 300) {
      throw `${response.status}: ${response.statusText}`;
    }
    return response.json();
  },
};
