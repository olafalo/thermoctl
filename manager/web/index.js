import App from './App';

// Load regenerator-runtime to allow transpilation of async Svelte functions
import 'regenerator-runtime/runtime';

new App({
  target: document.getElementById('svelte-app'),
});
