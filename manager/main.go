package main

import (
	"log"
	"net/http"

	"github.com/flick-web/dispatch"
)

func main() {
	go pollSensors()
	go stateControl()
	go keepHistoryFileClean()

	api := &dispatch.API{}
	api.AddEndpoint("GET/api/history", getHistory)
	api.AddEndpoint("GET/api/current", getCurrent)
	api.AddEndpoint("GET/api/settings", getSettings)
	api.AddEndpoint("POST/api/settings", updateSettings)

	http.HandleFunc("/api/", api.HTTPProxy)
	http.Handle("/", http.FileServer(http.Dir("web/dist")))
	log.Fatal(http.ListenAndServe(":80", nil))
}

// IndexData is the data structure sent to the main page template.
type IndexData struct {
	Records      []SensorRecord
	Settings     Settings
	Current      SensorRecord
	CurrentState string
	Errors       []string
}

func getHistory() []SensorRecord {
	return smoothedHistory(120, historyPeriod)
}

type currentStatus struct {
	Current      *SensorRecord `json:"current,omitempty"`
	CurrentState string        `json:"currentState,omitempty"`
}

func getCurrent() (*currentStatus, error) {
	currentData, err := getCurrentOverPeriod(60)
	if err != nil {
		return nil, err
	}
	return &currentStatus{
		Current:      currentData,
		CurrentState: lastState,
	}, nil
}

func getSettings() *Settings {
	return settings
}

func updateSettings(s *Settings) error {
	settings = s
	return settings.Save()
}
