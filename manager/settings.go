package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

var settings *Settings

func init() {
	// If settings doesn't exist, generate defaults and save
	settings = &Settings{}
	err := settings.Load()
	if err != nil {
		log.Println(err)
		settings = &Settings{
			MinTemp:       70,
			MaxTemp:       74,
			FanAlwaysOn:   false,
			MinSwitchFreq: 60 * 5,
			ConvertToF:    true,
		}
		err = settings.Save()
		if err != nil {
			log.Fatal(err)
		}
	}
}

// Settings represents the thermostat's current settings.
type Settings struct {
	// MinTemp is the temperature at which the AC will turn off, or the heat will turn on.
	MinTemp float64 `json:"minTemp"`
	// MaxTemp is the temperature at which the heat will turn off, or the AC will turn on.
	MaxTemp float64 `json:"maxTemp"`
	// FanAlwaysOn controls whether the blower fan is automatic or forced on.
	FanAlwaysOn bool `json:"fanAlwaysOn"`
	// MinSwitchFreq is the minimum amount of time, in seconds, that the manager
	// MUST wait before changing state.
	MinSwitchFreq int `json:"minSwitchFreq"`
	// Enabled controls whether the manager will control thermostat state
	Enabled bool `json:"enabled"`
	// ConvertToF controls whether temperature is converted to Fahrenheit
	ConvertToF bool `json:"convertToF"`
}

// Save writes the current settings to a file.
func (s *Settings) Save() error {
	data, err := json.Marshal(s)
	if err != nil {
		return err
	}
	return ioutil.WriteFile("settings.json", data, 0644)
}

// Load reads the settings from a file and loads the data.
func (s *Settings) Load() error {
	settings := Settings{}
	data, err := ioutil.ReadFile("settings.json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &settings)
	if err != nil {
		return err
	}
	*s = settings
	return nil
}
