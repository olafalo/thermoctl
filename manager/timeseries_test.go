package main

import (
	"testing"
	"time"
)

func TestTimeseries(t *testing.T) {
	ts := newSeriesBuffer(1 * time.Hour)
	ts.push(SensorRecord{
		UnixMS: timeToUnixMS(time.Now()),
		TempC:  1234,
	})
	if len(ts.getAll()) != 1 {
		t.Fail()
	}
	if ts.getLastN(1)[0].TempC != 1234 {
		t.Fail()
	}
	if len(ts.getLastN(10)) != 1 {
		t.Fail()
	}

	ts.push(SensorRecord{
		UnixMS: timeToUnixMS(time.Now()),
	})
	err := ts.push(SensorRecord{
		UnixMS: timeToUnixMS(time.Now().Add(-2 * time.Hour)),
	})
	if err == nil {
		t.Fail()
	}

	if len(ts.latestInTimeframe(time.Hour*2)) < 2 {
		t.Fail()
	}
}

func timeToUnixMS(t time.Time) int64 {
	return t.UnixNano() / 1e6
}
