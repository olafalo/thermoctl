#!/bin/bash

RPI=pi@192.168.1.50

set -e

function buildGoPkg() {
    cd $1
    GOOS=linux GOARCH=arm go build -o ../bin/$1 .
    cd $OLDPWD
}

# Build binaries
mkdir -p bin
buildGoPkg manager
buildGoPkg sensors
buildGoPkg switcher

ssh $RPI mkdir -p /home/pi/thermoctl/bin
ssh $RPI mkdir -p /home/pi/thermoctl/manager/web
ssh $RPI mkdir -p /tmp/thermoctl/

# Set up systemd services
scp -r systemd-services $RPI:/tmp/thermoctl/

SERVICES="thermoctl-switcher thermoctl-sensors thermoctl-manager"

# Stop running services
ssh $RPI "sudo systemctl stop $SERVICES"

# Upload binaries
scp -r bin/switcher $RPI:/home/pi/thermoctl/bin/switcher
scp -r bin/sensors $RPI:/home/pi/thermoctl/bin/sensors
scp -r bin/manager $RPI:/home/pi/thermoctl/bin/manager

# Build web files for production
cd manager/web
rm -rf dist
npm ci
parcel build index.html --experimental-scope-hoisting
cd -
# Upload web dist files
scp -r manager/web/dist/ $RPI:/home/pi/thermoctl/manager/web/dist/

# Enable/restart services
ssh $RPI "sudo cp -r /tmp/thermoctl/systemd-services/. /lib/systemd/system/ ;
sudo systemctl daemon-reload ;
sudo systemctl enable $SERVICES ;
sudo systemctl restart $SERVICES"
